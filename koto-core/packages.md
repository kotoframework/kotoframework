# Package com.kotoframework

Koto root package, contains Enums.kt and KotoApp.kt.

# Package com.kotoframework.beans

Some bean objects required in Koto, including result sets, Unkwnon objects, etc

# Package com.kotoframework.core

Koto core, including annotation, condition, etc.

# Package com.kotoframework.core.annotations

Some annotation configurations in Koto.

# Package com.kotoframework.core.condition

Some conditions in Koto.

# Package com.kotoframework.core.future

Provide Koto with some convenient features, such as from, table, etc.

# Package com.kotoframework.core.where

Used to generate various Where condition objects.

# Package com.kotoframework.definition

Type Definitions and Type Aliases.

# Package com.kotoframework.functions

All koto ORM functions for database and pojo classes.

# Package com.kotoframework.function.associate

Associate functions.

# Package com.kotoframework.function.create

Create functions.

# Package com.kotoframework.function.optionList

OptionList functions.

# Package com.kotoframework.function.remove

Remove functions.

# Package com.kotoframework.function.select

Select functions.

# Package com.kotoframework.function.statistic

Statistic functions.

# Package com.kotoframework.function.update

Update functions.

# Package com.kotoframework.interfaces

Interfaces for Koto.

# Package com.kotoframework.utils

Some utility classes for Koto.
